package HPC::Runner::Web::run::PBS;
use Data::Dumper;

use Moose;
extends qw(HPC::Runner::Web::Log HPC::Runner::PBS);
use namespace::autoclean;
use Cwd;

after 'check_meta' => sub {
    my $self = shift;
    my $line = shift;

    return unless $line =~ m/^#NOTE/;

    $self->add_batch( $line . "\n" );
};

sub process_batch_command {
    my $self = shift;
    my $command;

    my $counter = $self->batch_counter;
    $counter = sprintf( "%03d", $counter );

    #The gnuparallel option does not work very well...
    if ( $self->use_gnuparallel ) {
        $command = "cd " . getcwd() . "\n";
        $command
            .= "cat "
            . $self->cmdfile
            . " | parallelparser.pl | parallel --joblog "
            . $self->logdir
            . "/main.md --gnu -N 1 -q  gnuparallelrunner-web.pl --command `echo {}` --outdir "
            . $self->logdir
            . " --logname $counter\_"
            . $self->jobname
            . " --seq {#} --version "
            . $self->version
            . " --process_table "
            . $self->process_table;
        $command .= $self->custom_command;
        $command .= "\n";
    }
    elsif ( $self->has_custom_command ) {
        $command = "cd " . getcwd() . "\n";
        $command
            .= $self->custom_command
            . " --procs "
            . $self->procs
            . " --infile "
            . $self->cmdfile
            . " --outdir "
            . $self->logdir
            . " --logname $counter\_"
            . $self->jobname
            . " --process_table "
            . $self->process_table;
    }
    else {
        die print "None of the job processes were chosen!\n";
    }

    my $metastr = $self->create_meta_str;
    $command .= $metastr if $metastr;

    my $pluginstr = $self->create_plugin_str;
    $command .= $pluginstr if $pluginstr;

    return $command;
}

1;
