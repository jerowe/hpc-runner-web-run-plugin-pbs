package HPC::Runner::Web::run::Plugin::PBS;

use strict;
use 5.008_005;
our $VERSION = '0.07';
use Data::Dumper;

use Moose::Role;
use HPC::Runner::Web::run::PBS;
use Data::Dumper;

use namespace::autoclean;

sub run_notebook {
    my $self = shift;
    my $args = shift;
    my $tags = shift;

    $DB::single=2;
    my $pbs = HPC::Runner::Web::run::PBS->new($args);
    $pbs->tags($tags);
    $pbs->env_log;

    if($pbs->use_gnuparallel){
        $pbs->logfile("main.md");
        $pbs->init_log();
        $DB::single=2;
        $pbs->custom_command(" --tags ".join(',', @{$tags}));
    }
    else{
        $pbs->custom_command("mcerunner-web.pl --version ".$pbs->version." --tags ".join(',', @{$tags}));
    }
    $pbs->run();
}

1;
__END__

=encoding utf-8

=head1 NAME

HPC::Runner::Web::run::Plugin::PBS - Run your notebook through PBS

=head1 SYNOPSIS

    --plugins PBS

=head1 DESCRIPTION

HPC::Runner::Web::run::Plugin::PBS is a plugin for running your lab notebook through PBS. Please see the full documentation at L<HPC::Runner::PBS>.

=head1 AUTHOR

Jillian Rowe E<lt>jillian.e.rowe@gmail.comE<gt>

=head1 COPYRIGHT

Copyright 2015- Jillian Rowe

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=head1 SEE ALSO

=cut
