# NAME

HPC::Runner::Web::run::Plugin::PBS - Run your notebook through PBS

# SYNOPSIS

    --plugins PBS

# DESCRIPTION

HPC::Runner::Web::run::Plugin::PBS is a plugin for running your lab notebook through PBS. Please see the full documentation at [HPC::Runner::PBS](https://metacpan.org/pod/HPC::Runner::PBS).

# AUTHOR

Jillian Rowe &lt;jillian.e.rowe@gmail.com>

# COPYRIGHT

Copyright 2015- Jillian Rowe

# LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# SEE ALSO
