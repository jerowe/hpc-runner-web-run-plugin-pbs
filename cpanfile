requires 'perl', '5.008005';

requires "Moose::Role";
requires "HPC::Runner::Web::run::PBS";
requires "Data::Dumper";
requires "Moose";
requires "Cwd";
requires "HPC::Runner";
requires "HPC::Runner::Web";
requires "HPC::Runner::Web::Log";
requires "HPC::Runner::PBS";

# requires 'Some::Module', 'VERSION';

on test => sub {
    requires 'Test::Class::Moose';
};
