#!/usr/bin/env perl
package Main;

use Moose;
extends qw( HPC::Runner::Web::Log  HPC::Runner::GnuParallel);

use Data::Dumper;

has '+infile' =>(
    required => 0
);

has 'version' => (
    is => 'rw',
    isa => 'Str',
);

my $new = Main->new_with_options;
if($new->tags){
    my @tmp = @{$new->tags};
    $new->tags([]);
    foreach my $t (@tmp){
        my @s = split(',', $t);
        foreach my $s (@s){
            push(@{$new->tags}, $s);
        }
    }
}
$DB::single=2;

$new->go();
$DB::single=2;
#Main->new_with_options->go();

1;
