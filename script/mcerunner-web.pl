#!/usr/bin/env perl
package Main;

use Data::Dumper;
use HPC::Runner::Web::run::MCE;

my $new = HPC::Runner::Web::run::MCE->new_with_options();

if($new->tags){
    my @tmp = @{$new->tags};
    $new->tags([]);
    foreach my $t (@tmp){
        my @s = split(',', $t);
        foreach my $s (@s){
            push(@{$new->tags}, $s);
        }
    }
}

$new->go();
$DB::single=2;

1;
